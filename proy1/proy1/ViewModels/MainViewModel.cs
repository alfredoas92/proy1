﻿using System;
using System.Collections.Generic;
using System.Text;

namespace proy1.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using Models;
    using System.Windows.Input;

    public class MainViewModel
    {
        #region "PROPERTIES"

        public string Amount { get; set; }
        public IObservable<Rate> Rate { get; set; }
        public Rate SourceRate { get; set; }
        public Rate TargetRate { get; set; }
        public bool IsRunning { get; set; }
        public bool IsEnabled { get; set; }
        public string Result { get; set; }
        
        #endregion
        public MainViewModel()
        {

        }

        #region "Commands"
        public ICommand ConverterCommand
        {
            get
            {
                return new RelayCommand(Convert1);
            }
        }

        private void Convert1()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
